<?php
/**
 * UnderStrap functions and definitions
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// UnderStrap's includes directory.
$understrap_inc_dir = get_template_directory() . '/inc';

// Array of files to include.
$understrap_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker. Trying to get deeper navigation? Check out: https://github.com/understrap/understrap/issues/567.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
);

// Load WooCommerce functions if WooCommerce is activated.
if ( class_exists( 'WooCommerce' ) ) {
	$understrap_includes[] = '/woocommerce.php';
}

// Load Jetpack compatibility file if Jetpack is activiated.
if ( class_exists( 'Jetpack' ) ) {
	$understrap_includes[] = '/jetpack.php';
}

// Include files.
foreach ( $understrap_includes as $file ) {
	require_once $understrap_inc_dir . $file;
}

require_once TEMPLATEPATH . '/inc/acf/options.php';
require_once TEMPLATEPATH . '/inc/acf/tpl-sections.php';
require_once TEMPLATEPATH . '/inc/acf/post.php';
require_once TEMPLATEPATH . '/inc/acf/pxa-resource-type.php';
require_once TEMPLATEPATH . '/inc/acf/pxa-comm-activity.php';
require_once TEMPLATEPATH . '/inc/acf/pxa-comm-story.php';
// require_once TEMPLATEPATH . '/inc/acf/pxa-video.php';
require_once TEMPLATEPATH . '/inc/acf/pxa-icon-list.php';
require_once TEMPLATEPATH . '/inc/acf/page.php';

require_once TEMPLATEPATH . '/inc/acf/tpl-new-sections.php';

// 
// if ($_SERVER['SERVER_ADDR'] != '127.0.0.1') {
// 	define('ACF_LITE', true);
// }

require_once TEMPLATEPATH . '/inc/PXA_Admin.php';
require_once TEMPLATEPATH . '/inc/PXA_Helpers.php';
require_once TEMPLATEPATH . '/inc/PXA_Top_Bar_Menu_Walker.php';
