<?php
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_6081096f6166b',
        'title' => 'New sections template',
        'fields' => array(
            array(
                'key' => 'field_608109b36232d',
                'label' => 'Sections',
                'name' => 'new_sections',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'layouts' => array(
                    // these settings is for CTA section
                    '608109c161f24' => require TEMPLATEPATH . '/inc/acf/tpl-new-sections/cta.php',
                    // these settings is for demo section
                    'layout_608109fbd2724' => require TEMPLATEPATH . '/inc/acf/tpl-new-sections/demo.php',
                    'layout_son' => require TEMPLATEPATH . '/inc/acf/tpl-new-sections/son.php',
                )
            )
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
    
    endif;