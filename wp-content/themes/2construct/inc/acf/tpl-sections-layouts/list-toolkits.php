<?php
return array(
  'key' => 'layout_list-toolkits',
  'name' => 'list-toolkits',
  'label' => 'List Toolkits',
  'display' => 'block',
  'sub_fields' => array(
    array(
      'key' => 'field_5f1a5a9a714a5',
      'label' => 'Toolkits',
      'name' => 'toolkits',
      'type' => 'relationship',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'post_type' => array(
        0 => 'pxa-toolkit',
      ),
      'taxonomy' => '',
      'filters' => array(
        0 => 'search',
      ),
      'elements' => '',
      'min' => '',
      'max' => '',
      'return_format' => 'id',
    ),
  ),
  'min' => '',
  'max' => '',
);
