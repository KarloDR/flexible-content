<?php
return array(
  'key' => 'layout_5f1a5630d9db1',
  'name' => 'anchor',
  'label' => 'Anchor',
  'display' => 'row',
  'sub_fields' => array(
    array(
      'key' => 'field_5f1a563bd9db2',
      'label' => 'Anchor Name',
      'name' => 'anchor_name',
      'type' => 'text',
      'instructions' => 'Must be unique on the page',
      'required' => 1,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
  ),
  'min' => '',
  'max' => '',
);
