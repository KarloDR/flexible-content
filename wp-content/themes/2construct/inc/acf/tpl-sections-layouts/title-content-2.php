<?php
return array(
  'key' => 'layout_title-content-2',
  'name' => 'title-content-2',
  'label' => 'Title - Content (Blue BG)',
  'display' => 'block',
  'sub_fields' => array(
    array(
      'key' => 'field_5f1a5637502a1',
      'label' => 'Title',
      'name' => 'title',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
    array(
      'key' => 'field_5f1a5637502a2',
      'label' => 'Subtitle',
      'name' => 'subtitle',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
    array(
      'key' => 'field_5f1a5637502a3',
      'label' => 'Content',
      'name' => 'content',
      'type' => 'wysiwyg',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'tabs' => 'all',
      'toolbar' => 'full',
      'media_upload' => 0,
      'delay' => 0,
    ),
  ),
  'min' => '',
  'max' => '',
);
