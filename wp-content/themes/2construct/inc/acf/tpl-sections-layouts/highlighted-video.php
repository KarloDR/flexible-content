<?php
return array(
  'key' => 'layout_highlighted-video',
  'name' => 'highlighted-video',
  'label' => 'Highlighted Video',
  'display' => 'block',
  'sub_fields' => array(
    array(
      'key' => 'field_5ff52afc85681',
      'label' => 'Video Resource',
      'name' => 'video_resource',
      'type' => 'post_object',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'post_type' => array(
        0 => 'post',
      ),
      'taxonomy' => '',
      'allow_null' => 0,
      'multiple' => 0,
      'return_format' => 'id',
      'ui' => 1,
    ),
  ),
  'min' => '',
  'max' => '',
);
