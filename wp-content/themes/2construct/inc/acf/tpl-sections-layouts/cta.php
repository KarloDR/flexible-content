<?php

return 			array(
	'key' => 'field_607fb85bfb5c0',
	'label' => 'CA Section',
	'name' => 'ca_section',
	'type' => 'flexible_content',
	'instructions' => '',
	'required' => 0,
	'conditional_logic' => 0,
	'wrapper' => array(
		'width' => '',
		'class' => '',
		'id' => '',
	),
	'layouts' => array(
		'607fb868935c6' => array(
			'key' => '607fb868935c6',
			'name' => 'layout_cta',
			'label' => 'Layout CTA',
			'display' => 'block',
			'sub_fields' => array(
			),
			'min' => '',
			'max' => '',
		),
	),
	'button_label' => 'Add Row',
	'min' => '',
	'max' => '',
);