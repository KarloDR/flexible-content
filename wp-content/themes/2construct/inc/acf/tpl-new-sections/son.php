<?php
return array(
    'key' => 'layout_608109c161ac1', // we need unique key here
    'name' => 'son', // name of the layout
    'label' => 'Son Section',
    'display' => 'block',
    'sub_fields' => array(
        array(
            'key' => 'field_a_random_string',
            'label' => 'Headline',
            'name' => 'headline',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'min' => '',
    'max' => '',
);