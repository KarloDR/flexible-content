<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
  'key' => 'group_5fe86c8c918a1',
  'title' => 'Video Details',
  'fields' => array(
    array(
      'key' => 'field_5fe86c8c918a2',
      'label' => 'Video',
      'name' => 'video',
      'type' => 'oembed',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'width' => '',
      'height' => '',
    ),
  ),
  'location' => array(
    array(
      array(
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'pxa-video',
      ),
    ),
  ),
  'menu_order' => 100,
  'position' => 'normal',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => 1,
  'description' => '',
));

endif;
