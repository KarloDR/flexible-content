<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
  'key' => 'group_5e9582b360901',
  'title' => 'General',
  'fields' => array(
    array(
      'key' => 'field_5e95b95c70ff2',
      'label' => 'Before &lt;head&gt; closing tag scripts',
      'name' => 'ca_head_scripts',
      'type' => 'acf_code_field',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'mode' => 'htmlmixed',
      'theme' => 'monokai',
    ),
    array(
      'key' => 'field_5e95bac969961',
      'label' => 'After &lt;body&gt; opening tag scripts',
      'name' => 'ca_after_body_scripts',
      'type' => 'acf_code_field',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'mode' => 'htmlmixed',
      'theme' => 'monokai',
    ),
    array(
      'key' => 'field_5e95baf169962',
      'label' => 'Before &lt;body&gt; closing tag scripts',
      'name' => 'ca_foot_scripts',
      'type' => 'acf_code_field',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'mode' => 'htmlmixed',
      'theme' => 'monokai',
    ),
    array(
      'key' => 'field_5ea2bbb58e47f',
      'label' => 'Logo',
      'name' => 'ca_logo',
      'type' => 'image',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'return_format' => 'array',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'min_width' => '',
      'min_height' => '',
      'min_size' => '',
      'max_width' => '',
      'max_height' => '',
      'max_size' => '',
      'mime_types' => '',
    ),
    array(
      'key' => 'field_5ec928a16ef1f',
      'label' => 'Main Website URL',
      'name' => 'disaster_hub_website_url',
      'type' => 'url',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
    ),
    // array(
    //   'key' => 'field_5eb2c62eb016f',
    //   'label' => 'Contact Email',
    //   'name' => 'ca_contact_email',
    //   'type' => 'email',
    //   'instructions' => '',
    //   'required' => 0,
    //   'conditional_logic' => 0,
    //   'wrapper' => array(
    //     'width' => '',
    //     'class' => '',
    //     'id' => '',
    //   ),
    //   'default_value' => '',
    //   'placeholder' => '',
    //   'prepend' => '',
    //   'append' => '',
    //   'maxlength' => '',
    // ),
    // array(
    //   'key' => 'field_5eb2c62eb016a',
    //   'label' => 'Contact Phone',
    //   'name' => 'ca_contact_phone',
    //   'type' => 'text',
    //   'instructions' => '',
    //   'required' => 0,
    //   'conditional_logic' => 0,
    //   'wrapper' => array(
    //     'width' => '',
    //     'class' => '',
    //     'id' => '',
    //   ),
    //   'default_value' => '',
    //   'placeholder' => '',
    //   'prepend' => '',
    //   'append' => '',
    //   'maxlength' => '',
    // ),
    // array(
    //   'key' => 'field_5eda5f8c4901a',
    //   'label' => 'Search - No results message',
    //   'name' => 'ca_search_no_results_msg',
    //   'type' => 'wysiwyg',
    //   'instructions' => '',
    //   'required' => 0,
    //   'conditional_logic' => 0,
    //   'wrapper' => array(
    //     'width' => '',
    //     'class' => '',
    //     'id' => '',
    //   ),
    //   'default_value' => '',
    //   'tabs' => 'all',
    //   'toolbar' => 'advanced',
    //   'media_upload' => 1,
    //   'delay' => 0,
    // ),
    array(
      'key' => 'field_5eb2c62c8a74a',
      'label' => '404 Page Title',
      'name' => 'ca_404_title',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
    array(
      'key' => 'field_5eda5f87ea64f',
      'label' => '404 Page Content',
      'name' => 'ca_404_content',
      'type' => 'wysiwyg',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'tabs' => 'all',
      'toolbar' => 'advanced',
      'media_upload' => 1,
      'delay' => 0,
    ),
  ),
  'location' => array(
    array(
      array(
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'ca-settings',
      ),
    ),
  ),
  'menu_order' => 1,
  'position' => 'normal',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => 1,
  'description' => '',
));

acf_add_local_field_group(array(
  'key' => 'group_5e95bbafda81c',
  'title' => 'Footer Newsletter Section',
  'fields' => array(
    array(
      'key' => 'field_5e95b958a71a2',
      'label' => 'Section Title',
      'name' => 'ca_footer_newsletter_title',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
    array(
      'key' => 'field_5e95b958a71a3',
      'label' => 'Section Subtitle',
      'name' => 'ca_footer_newsletter_subtitle',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
    array(
      'key' => 'field_5e95b958a71a1',
      'label' => 'Form shortcode',
      'name' => 'ca_footer_newsletter_shortcode',
      'type' => 'acf_code_field',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'mode' => 'htmlmixed',
      'theme' => 'monokai',
    ),
  ),
  'location' => array(
    array(
      array(
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'ca-settings',
      ),
    ),
  ),
  'menu_order' => 3,
  'position' => 'normal',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => 1,
  'description' => '',
));

acf_add_local_field_group(array(
  'key' => 'group_5e95bbafd47df',
  'title' => 'Footer',
  'fields' => array(
    array(
      'key' => 'field_5e95b95c8a721',
      'label' => 'Back to main website link label',
      'name' => 'ca_footer_back_site_label',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => 'Back To Main Website',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
    array(
      'key' => 'field_5eb2c61bb0169',
      'label' => 'Logo',
      'name' => 'ca_footer_logo',
      'type' => 'image',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'return_format' => 'object',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'min_width' => '',
      'min_height' => '',
      'min_size' => '',
      'max_width' => '',
      'max_height' => '',
      'max_size' => '',
      'mime_types' => '',
    ),
    array(
      'key' => 'field_5eda5f8c72ad2',
      'label' => 'Copyright',
      'name' => 'ca_copyright',
      'type' => 'wysiwyg',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'tabs' => 'all',
      'toolbar' => 'basic',
      'media_upload' => 0,
      'delay' => 0,
    ),
    array(
      'key' => 'field_5f1690dca918a',
      'label' => 'Default Footer FAQs',
      'name' => 'default_footer_faqs',
      'type' => 'relationship',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'post_type' => array(
        0 => 'pxa-faq',
      ),
      'taxonomy' => '',
      'filters' => array(
        0 => 'search',
      ),
      'elements' => array(
      ),
      'min' => '',
      'max' => '',
      'return_format' => 'id',
    ),
    array(
      'key' => 'field_5eb2c61c8a714',
      'label' => 'Bottom Right Logo',
      'name' => 'ca_footer_logo_bottom_right',
      'type' => 'image',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'return_format' => 'object',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'min_width' => '',
      'min_height' => '',
      'min_size' => '',
      'max_width' => '',
      'max_height' => '',
      'max_size' => '',
      'mime_types' => '',
    ),
    array(
      'key' => 'field_5eb2c61c8a715',
      'label' => 'Bottom Right Logo Link',
      'name' => 'ca_footer_logo_bottom_right_link',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
  ),
  'location' => array(
    array(
      array(
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'ca-settings',
      ),
    ),
  ),
  'menu_order' => 3,
  'position' => 'normal',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => 1,
  'description' => '',
));

acf_add_local_field_group(array(
  'key' => 'group_5ed102519ab3b',
  'title' => 'Social Media',
  'fields' => array(
    array(
      'key' => 'field_5ee81f8d6ef1e',
      'label' => 'Social Network',
      'name' => 'ca_socials',
      'type' => 'repeater',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'collapsed' => 'field_5ee820786ef21',
      'min' => 0,
      'max' => 0,
      'layout' => 'table',
      'button_label' => 'Add Item',
      'sub_fields' => array(
        array(
          'key' => 'field_5ee820786ef21',
          'label' => 'Name',
          'name' => 'name',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5ee820276ef1f',
          'label' => 'URL',
          'name' => 'url',
          'type' => 'url',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
        ),
        // array(
        //   'key' => 'field_5ee820646ef20',
        //   'label' => 'Icon',
        //   'name' => 'icon',
        //   'type' => 'image',
        //   'instructions' => '',
        //   'required' => 0,
        //   'conditional_logic' => 0,
        //   'wrapper' => array(
        //     'width' => '',
        //     'class' => '',
        //     'id' => '',
        //   ),
        //   'return_format' => 'url',
        //   'preview_size' => 'thumbnail',
        //   'library' => 'all',
        //   'min_width' => '',
        //   'min_height' => '',
        //   'min_size' => '',
        //   'max_width' => '',
        //   'max_height' => '',
        //   'max_size' => '',
        //   'mime_types' => '',
        // ),
      ),
    ),
  ),
  'location' => array(
    array(
      array(
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'ca-settings',
      ),
    ),
  ),
  'menu_order' => 4,
  'position' => 'normal',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => 1,
  'description' => '',
));

acf_add_local_field_group(array(
  'key' => 'group_5ed102519ac1a',
  'title' => 'Pages & Templates',
  'fields' => array(
    array(
      'key' => 'field_5f2d3aec8a741',
      'label' => 'Resources Page',
      'name' => 'page_resources',
      'type' => 'post_object',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'post_type' => array(
        0 => 'page',
      ),
      'taxonomy' => '',
      'allow_null' => 1,
      'multiple' => 0,
      'return_format' => 'id',
      'ui' => 1,
    ),
    array(
      'key' => 'field_5ff52afc8a712',
      'label' => 'Video Library Page',
      'name' => 'page_video_library',
      'type' => 'page_link',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'post_type' => array(
        0 => 'page',
      ),
      'taxonomy' => '',
      'allow_null' => 0,
      'allow_archives' => 1,
      'multiple' => 0,
    ),
    array(
      'key' => 'field_5f2d3aec8a742',
      'label' => 'Video Resource Type',
      'name' => 'video_resource_type',
      'type' => 'post_object',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'post_type' => array(
        0 => 'pxa-resource-type',
      ),
      'taxonomy' => '',
      'allow_null' => 1,
      'multiple' => 0,
      'return_format' => 'id',
      'ui' => 1,
    ),
  ),
  'location' => array(
    array(
      array(
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'ca-settings',
      ),
    ),
  ),
  'menu_order' => 5,
  'position' => 'normal',
  'style' => 'default',
  'label_placement' => 'left',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => 1,
  'description' => '',
));

endif;
