<div>
    <h3>This is CTA section</h3>
    <!-- We could load their own sub fields from here -->
    <a href="<?php the_sub_field('url') ?>"><?php the_sub_field('title') ?></a>
    <hr>
</div>