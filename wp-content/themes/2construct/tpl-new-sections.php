<?php
/**
 * Template Name: New Sections
 */
get_header();?>

<main id="main">
	<?php
	if (have_posts()) :
		while (have_posts()) :
			the_post();
			?>

			<?php if ( have_rows('new_sections') ) : ?>
			<!-- Sections Start -->
			<section class="ca-sections">
				<?php
				$sectionCounter = 0;
				while ( have_rows('new_sections') ) :
					the_row();
					$sectionLayout = get_row_layout();
                    // I create individual template file for each layout
					?>
                    <?php get_template_part('sections/' . $sectionLayout); ?>
                    <!-- <div>
                        <h3>This is <?php echo $sectionLayout; ?> section</h3>
                        <p>On demo section, we have sub fields "image" we will load it.</p>
                        <p><img src="<?php echo get_sub_field('image'); ?>"></p>
                        <p>On cta section, we have "title" & "url"</p>
                        <?php the_sub_field('title'); ?><br>
                        <?php the_sub_field('url'); ?>
                        <hr>
                    </div> -->
				<?php endwhile; ?>
			</section>
			<!-- Sections End -->
		<?php endif; ?>
		<?php
		endwhile;
	endif;
	?>
</main>


<?php get_footer(); ?>