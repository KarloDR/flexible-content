<section <?php PXA_Helpers::SectionAttrs('faqs'); ?>>
  <div class="container">
    <?php if ( $title = get_sub_field('title') ) : ?>
      <h2 class="st-title text-primary"><?php echo $title; ?></h2>
    <?php endif; ?>
    <div class="desc format">
      <?php the_sub_field('description'); ?>
    </div>

    <div class="faqs-wrapper">
      <div class="left-col"></div>
      <div class="right-col"></div>
    </div>

    <div class="faqs-loader"></div>
  </div>
</section>
