<?php if ( function_exists('bcn_display') && !get_field('page_breadcrumb_disabled') ) : ?>
  <section class="st-breadcrumb<?php echo get_field('page_breadcrumb_light') ? ' light' : '' ?>">
    <div class="container">
      <div class="inn">
        <div class="breadcrumb-wrapper"><?php bcn_display(); ?></div>
      </div>
    </div>
  </section>
<?php endif; ?>
