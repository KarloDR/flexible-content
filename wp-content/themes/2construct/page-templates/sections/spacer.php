<?php
$bgcolor = get_sub_field('bg_color');
$height_mob = (int) get_sub_field('height_sm');
$height_tab = (int) get_sub_field('height_md');
$height_pc = (int) get_sub_field('height_lg');
?>
<section <?php PXA_Helpers::SectionAttrs('spacer'); ?>>
  <div class="d-block d-md-none" style="height: <?php echo $height_mob; ?>px;"></div>
  <div class="d-none d-md-block d-lg-none" style="height: <?php echo $height_tab; ?>px;"></div>
  <div class="d-none d-lg-block" style="height: <?php echo $height_pc; ?>px;"></div>
</section>
