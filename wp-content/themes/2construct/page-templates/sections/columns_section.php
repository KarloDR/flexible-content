<section <?php PXA_Helpers::SectionAttrs('content');  ?>>
	<div class="container">
		<div class="content format" style="max-width: <?php the_sub_field('width'); ?>%">
			<div class="contact-page">
				<h1><?= get_sub_field('page_title_2') ?></h1>
			</div>
			<div class="vertical-separator"><br><br></div>


			<div class="row">
				<div class="col-lg-6 text-field" style="font-weight: 300!important; font-size: 19px!important; padding-right: 6rem!important;">
					<p><?= get_sub_field('page_description_2') ?></p>
				</div>

				<div class="col-lg-6" style="border-left: 1px solid #F47721">
					<?php if (have_rows('contact_info_2')): ?>
						<ul>
							<?php while (have_rows('contact_info_2')) : the_row(); ?>
								<li class="font-weight-bold"><?php the_sub_field('contact_title_2'); ?></li>
								<li><?php the_sub_field('contact_value_2'); ?></li>
							<?php endwhile; ?>
						</ul>
					<?php else : ?>
						<p>No todos found.</p>
					<?php endif; ?>
				</div>
			</div>

			<div class="vertical-separator"><br><br><br></div>

			<div class="row">
				<div class="col-lg-6 maps">
					<img src="<?= get_sub_field('image_1') ?>" alt=""><br><br>
					<label>BRIGHTON OFFICE</label>
					<h1>Level 2, 36 Carpenter Street,
						Brighton VIC 3186</h1>
				</div>
				<div class="col-lg-6 maps">
					<img src="<?= get_sub_field('image_1') ?>" alt=""><br><br>
					<label>BRIGHTON OFFICE</label>
					<h1>Level 2, 36 Carpenter Street,
						Brighton VIC 3186</h1>
				</div>
			</div>

		</div>
	</div>
</section>
