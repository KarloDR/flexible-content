<section <?php PXA_Helpers::SectionAttrs('title-content'); ?>>
  <div class="container">
    <?php if ( $title = get_sub_field('title') ) : ?>
      <h1 class="st-title text-primary"><?php echo $title; ?></h1>
    <?php endif; ?>
    <div class="content format">
      <?php the_sub_field('content'); ?>
    </div>
  </div>
</section>
