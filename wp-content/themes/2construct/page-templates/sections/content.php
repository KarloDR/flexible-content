<section <?php PXA_Helpers::SectionAttrs('content'); ?>>
  <div class="container">
    <div class="content format" style="max-width: <?php the_sub_field('width');?>%">
      <?php the_sub_field('content'); ?>
    </div>
  </div>
</section>
