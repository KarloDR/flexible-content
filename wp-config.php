<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'flexible_content_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3307' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'fb0(S|YR{bJ~wZ|&tFEIM]vheZ.5XlLvbZulCx;wLbPM=]H^d{6Cm{7v5^x_d9:h' );
define( 'SECURE_AUTH_KEY',  'XO [1X.1]Y!WerlQ?(e^|!R;r4c%zo3WPir#CO89?:8ox=-Evcs8KX[b~t-35-D%' );
define( 'LOGGED_IN_KEY',    ')42nAolQm0%s?fy! X08F5HqxnVuY};AV!M#Jz0NhHCgw)N+@9FE;#n77yT|}CD$' );
define( 'NONCE_KEY',        'pGT5Ve].o4nND2A0l>q`Ivg.CD7(q{UVx<GP]umJ9=v<9wkMI}|Pet%~b;%)r7n{' );
define( 'AUTH_SALT',        '6hi.x7id|50^L+$%2@lNvi<?Rp%3nsQMg4{>*Sk{@$Kq>N!ixi#BqbI1P:Bkz:C ' );
define( 'SECURE_AUTH_SALT', 'GkLo]3kcX`T(?%rcUul=#OpS3iEv,qH`XO(cVDHe<o1wu$C 9X~cFhtat]](N}#3' );
define( 'LOGGED_IN_SALT',   '[G9B0^T]/gaQAho I(U98Pq6`4dzF43:#Rd2ss?>RhIK;Ymg@r`:sMJ:NQ*otN78' );
define( 'NONCE_SALT',       'n9+i{HV^~$*m>~_X_LJj!zS{iq &gORo9HbPEC>L!MP.(dhH#-N(9v :wxt8MhOv' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
